const Users_URL = "https://ajax.test-danit.com/api/json/users";
const Posts_URL = "https://ajax.test-danit.com/api/json/posts";
const root = document.querySelector("#root");

function getRequest(url) {
  return fetch(url)
    .then((response) => response.json())
    .then((data) => {
      return data;
    })
    .catch((error) => console.error(error));
}

function deletePlease(id) {
  return fetch("https://ajax.test-danit.com/api/json/posts/" + id, {
    method: "DELETE",
  }).then((response) => {
    return response.ok;
  });
}

async function CombineRequests() {
  return {
    users: await getRequest(Users_URL),
    posts: await getRequest(Posts_URL),
  };
}

CombineRequests().then((data) => {
  let posts = data.posts;
  let users = data.users;

  posts.forEach((post) => {
    let userWeFound = users.find((el) => {
      if (el.id === post.userId) {
        return true;
      }
    });
    post.name = userWeFound.name;
    post.username = userWeFound.username;
    post.email = userWeFound.email;

    const card = new Card(post);
    root.append(card.render());
  });

  console.log(posts);
});
// getRequest(Users_URL).then((data) => {
//   data.forEach((el) => {
//     const card = new Card(el);
//     root.append(card.render());
//   });
// });

class Card {
  constructor({ name, username, email, title, body, id }) {
    this.email = email;
    this.name = name;
    this.username = username;
    this.title = title;
    this.body = body;
    this.id = id;
  }
  render() {
    const div = document.createElement("div");
    div.className = "card";
    div.dataset.cardId = this.id;
    const h3 = document.createElement("h3");
    h3.textContent = this.title;
    div.append(h3);
    const p = document.createElement("p");
    p.className = "name";
    p.textContent = `Username: ${this.username}, Name: ${this.name}, Email: ${this.email}`;
    div.append(p);

    const p2 = document.createElement("p");
    p2.className = "text";
    p2.textContent = this.body;
    div.append(p2);

    const btn = document.createElement("button");
    btn.textContent = "❌";
    div.append(btn);

    btn.addEventListener("click", (e) => {
      let id = e.target.closest(".card").dataset.cardId;
      deletePlease(id).then((del) => {
        if (del) {
          e.target.closest(".card").remove();
        }
      });
    });
    return div;
  }
}
