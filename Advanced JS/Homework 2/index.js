"use strict";

// Теоретичне питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch:

// 1. При написанні коду для того щоб скрипт продовжив роботу у разі наявності помилок
// 2. Для відслідковування помилок

// Завдання
// Дано масив books.

// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const ulList = document.createElement("ul");
ulList.id = "list";
const div = document.querySelector("#root");
div.append(ulList);

const goodBooks = books.filter((el) => {
  try {
    if (!el.author) {
      throw new Error("no author");
    } else if (!el.name) {
      throw new Error("no name");
    } else if (!el.price) {
      throw new Error("no price");
    } else {
      return el;
    }
  } catch (error) {
    console.error(error);
  }
});

class Book {
  constructor(author, name, price) {
    this.author = author;
    this.name = name;
    this.price = price;
  }
  render() {
    const li = document.createElement("li");
    li.textContent = `${this.author} ${this.name} ${this.price}`;

    return li;
  }
}

goodBooks.forEach((e) => {
  let qwerty = new Book(e.author, e.name, e.price);
  ulList.append(qwerty.render());
});
