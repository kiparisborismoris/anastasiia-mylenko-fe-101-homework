const root = document.querySelector("#root");
const btn = document.createElement("button");
btn.textContent = "💥";
root.append(btn);

const URL = "https://api.ipify.org/?format=json";
let IP_URL = "http://ip-api.com/json/";

async function getRequest() {
  return await fetch(URL)
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      return data;
    })
    .catch((error) => console.error(error));
}

async function getAPILocation({ ip }) {
  console.log(ip);

  return await fetch(IP_URL + ip)
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      return data;
    });
}

btn.addEventListener("click", async () => {
  await getRequest()
    .then(getAPILocation)
    .then((data) => {
      const test = new IPFinder(data);
      test.render();
    });
});

class IPFinder {
  constructor({ country, timezone, regionName, zip }) {
    this.country = country;
    this.timezone = timezone;
    this.regionName = regionName;
    this.zip = zip;
  }
  render() {
    const p = document.createElement("p");
    p.className = "text";
    p.textContent = `your country is: ${this.country} + your timezone is: ${this.timezone}  ${this.regionName}  ${this.zip}`;
    root.append(p);

    return p;
  }
}
