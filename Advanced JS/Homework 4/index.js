// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни +
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).+
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

const URL = "https://ajax.test-danit.com/api/swapi/films";
const root = document.querySelector("#root");

function getRequest(url) {
  return fetch(url).then((response) => {
    return response.json();
  });
}

class Films {
  constructor({ episodeId, name, openingCrawl }) {
    this.episodeId = episodeId;
    this.name = name;
    this.openingCrawl = openingCrawl;
  }
  render() {
    const li = document.createElement("li");
    li.innerHTML = `<p>id-${this.episodeId}</p><p>name-${this.name}</p><p>description-${this.openingCrawl}</p><p class='CharactersName'> Characters: </p>`;
    li.dataset.episodeId = this.episodeId;
    return li;
  }
}

getRequest(URL)
  .then((data) => {
    const ul = document.createElement("ul");
    root.append(ul);
    console.log(data);
    data.forEach((el) => {
      let li = new Films(el);
      console.log(li);
      ul.append(li.render());
    });
    return data;
  })
  .then((data) => {
    data.forEach((el) => {
      let target = document.querySelector(
        `[data-episode-id="${el.episodeId}"]>.CharactersName`
      );
      el.characters.forEach((character) => {
        getRequest(character).then((res) => {
          target.textContent = target.textContent + res.name;
        });
      });
    });
  });
