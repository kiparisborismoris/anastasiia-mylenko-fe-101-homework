"use strict";

// Теоретичне питання
// 1.) Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

// Если нам надо испльзовать какое-то свойство которое отсутсвует, прототип (специальное скрытое свойство) автоматически возьмёт его из объекта.
// Это помогает повторно использовать то что есть у первого\начального объекта, а не копировать всё.

// 2.) Для чого потрібно викликати super() у конструкторі класу-нащадка?

// Аби передати дані від класу батька не переписуючи кожен рядок. super() дозволяє скоротити запис і не перезаписати, а передати інформацію у класі-нащадку

// Завдання
// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  set name(value) {
    this._name = value;
  }
  get name() {
    return this._name;
  }
  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }
  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  set salary(value) {
    this._salary = value;
  }

  get salary() {
    return this._salary * 3;
  }
}

let jsDev = new Programmer("Buba", "25", "3000", "JS");
console.log(jsDev);
console.log(jsDev.salary);
let javaDev = new Programmer("Boba", "27", "4000", "Java");
console.log(javaDev);
console.log(javaDev.salary);
let cPlusDev = new Programmer("Biba", "31", "5000", "C++");
console.log(cPlusDev);
console.log(cPlusDev.salary);
