import { Component } from "react";
import Button from "../Button/Button";

class Modal extends Component {
  render() {
    return (
      <div
        className="overlay"
        onClick={(event) => {
          if (!event.target.closest(".modal")) {
            this.props.closeModal();
          }
        }}
      >
        <div className={this.props.modal__styles}>
          <div className="modal__content">
            <h2 className="modal__title">{this.props.title}</h2>
            <p className="modal__text">{this.props.text}</p>
            <Button styles={this.props.btn__styles + " btn__good"} text="Ok" />
            <Button
              styles={this.props.btn__styles + " btn_nogood"}
              text="Cancel"
              action={this.props.closeModal}
            />
            {this.props.closeModalBtn && (
              <button onClick={this.props.closeModal} className="modal__close">
                ❌
              </button>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
