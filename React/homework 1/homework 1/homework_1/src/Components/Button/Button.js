import { Component } from "react";
import "./Button.scss";

class Button extends Component {
  render() {
    return (
      <>
        <button
          className={this.props.styles }
          type="button"
          onClick={this.props.action}
        >
          {this.props.text}
        </button>
      </>
    );
  }
}

export default Button;
