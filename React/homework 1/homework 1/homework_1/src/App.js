import { Component } from "react";
import Button from "./Components/Button/Button";
import Modal from "./Components/Modal/Modal";

class App extends Component {
  state = {
    modal1: false,
    modal2: false,
  };
  handlerOpenModal = (target) => {
    this.setState((prev) => (prev[target] = true));
  };
  // handlerOpenModal2 = () => {
  //   this.setState((prev) => (prev.modal2 = true));
  // };

  handlerCloseModals = () => {
    this.setState(() => ({
      modal1: false,
      modal2: false,
    }));
  };

  render() {
    return (
      <div className="container">
        <Button
          styles="btn btn__text"
          text="Open first modal"
          action={() => this.handlerOpenModal("modal1")}
        />
        <Button
          styles="btn btn__text "
          text="Open second modal"
          action={() => this.handlerOpenModal("modal2")}
        />
        {this.state.modal1 && (
          <Modal
            modal__styles="modal modal1"
            btn__styles=""
            closeModal={this.handlerCloseModals}
            closeModalBtn={true}
            title="Do you want to delete this file?"
            text="Juan Ponce de León había nacido el 8 de abril de 14602​ en Santervás de Campos, localidad de la provincia de Valladolid."
          />
        )}
        {this.state.modal2 && (
          <Modal
            modal__styles="modal modal2 "
            btn__styles="btn__styles"
            title="Modal 2"
            text="En su estancia en Higüey, escuchó las historias de las riquezas existentes en Borinquén, la Isla de San Juan. A partir de ese momento concentró todos sus empeños en poder acudir a ese sitio, siéndole concedido el permiso necesario. "
            closeModal={this.handlerCloseModals}
            closeModalBtn={false}
          />
        )}
      </div>
    );
  }
}

export default App;
