import { useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import Header from "./components/Header/Header";
import CatList from "./components/CatList/CatList";
import DoggoPage from "./Pages/DoggoPage/DoggoPage";
import CartPage from "./Pages/CartPage/CartPage";
import FavPage from "./Pages/FavPage/FavPage";
import NotPage from "./Pages/NotPage/NotPage";
import Footer from "./components/Footer/Footer";

function App() {
  const [favList, SetFavList] = useState([]);
  const [cartList, SetCartList] = useState([]);
  const [cats, SetCats] = useState([]);
  const [isLoaded, SetIsLoaded] = useState(false);
  const [modal, SetModal] = useState(false);
  const [activeCart, SetActiveCart] = useState("");

  function favStorageHandler(value) {
    localStorage.setItem("favList", JSON.stringify(value));
  }

  function cartStorageHandler(value) {
    localStorage.setItem("cartList", JSON.stringify(value));
  }

  // Star localStorage
  function favListHandler(e) {
    const id = e.target.closest(".cat__card").id;

    SetFavList((prev) => {
      let newList = [];
      if (!prev.includes(id)) {
        newList = [...prev];
        newList.push(id);
        favStorageHandler(newList);
        return newList;
      } else {
        newList = prev.filter((el) => {
          if (el !== id) {
            return el;
          }
        });
        favStorageHandler(newList);
        return newList;
      }
    });
  }

  useEffect(() => {
    fetch("./cats/cats.json")
      .then((res) => res.json())
      .then((json) => {
        SetCats(json);
        SetIsLoaded(true);
      })
      .catch((err) => {
        console.log(err);
      });

    let getfavList = localStorage.getItem("favList");
    if (getfavList !== null) {
      getfavList = JSON.parse(getfavList);

      SetFavList(() => {
        return getfavList;
      });
    }

    // same for Add to Cart
    let getCartList = localStorage.getItem("cartList");
    if (getCartList !== null) {
      getCartList = JSON.parse(getCartList);
      SetCartList(() => {
        return getCartList;
      });
    }
  }, []);

  // Cart localStorage
  function cartListHandler(id) {
    SetCartList((prevCartList) => {
      let newCartList = [...prevCartList];
      if (!prevCartList.includes(id)) {
        newCartList.push(id);
        cartStorageHandler(newCartList);
        return newCartList;
      } else {
        return prevCartList;
      }
    });
  }

  // Delete item from cart
  function deleteItem(e) {
    const id = e.target.closest(".cat__card").id;
    SetCartList((prevState) => {
      let newCartList = [...prevState];
      newCartList = newCartList.filter((el) => {
        if (el !== id) {
          return el;
        }
      });
      cartStorageHandler(newCartList);
      return newCartList;
    });
  }

  return (
    <>
      <Header counter={cartList.length} starCounter={favList} />
      <Routes>
        <Route path="/">
          <Route
            index
            element={
              <>
                <CatList
                  className="main__page"
                  favListHandler={favListHandler}
                  favList={favList}
                  cartListHandler={cartListHandler}
                  cartList={cartList}
                  cats={cats}
                  isLoaded={isLoaded}
                  modal={modal}
                  activeCart={activeCart}
                  SetModal={SetModal}
                  SetActiveCart={SetActiveCart}
                />
                <DoggoPage />
              </>
            }
          />
          <Route
            path="shopingCart"
            element={
              <CartPage
                deleteItem={deleteItem}
                cats={cats.filter((kitten) => {
                  if (cartList.includes(kitten.id)) {
                    return kitten;
                  }
                })}
              />
            }
          />
          <Route path="doggo" element={<DoggoPage />} />
          <Route
            path="favorites"
            element={
              <FavPage
                favListHandler={favListHandler}
                favList={favList}
                cats={cats.filter((favKitten) => {
                  if (favList.includes(favKitten.id)) {
                    return favKitten;
                  }
                })}
              />
            }
          />
          <Route path="*" element={<NotPage />} />
        </Route>
      </Routes>

      <Footer />
    </>
  );
}

export default App;
