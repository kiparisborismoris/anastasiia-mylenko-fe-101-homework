import CatCard from "../CatCard/CatCard";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";

import "swiper/css";
import "swiper/css/navigation";
import "./CatList.scss";

function CatList(props) {
  const {
    cats,
    isLoaded,
    modal,
    activeCart,
    SetModal,
    SetActiveCart,
  } = props;

  const updateActiveCart = (value) => {
    SetActiveCart(value);
  };


  const handlerOpenModal = () => {
    SetModal(true);
  };

  const handlerCloseModals = () => {
    SetModal(false);
  };

  if (!isLoaded) return <div className="error__load--state">Loading...</div>;

  return (
    <>
      <h1 className="catList__title">Kittens</h1>
      <Swiper
        navigation={true}
        className="catList__container"
        spaceBetween={50}
        slidesPerView={4}
        modules={[Navigation]}
      >
        <ul className="cats__container">
          {cats.map((cats) => (
            <SwiperSlide key={cats.id}>
              <CatCard
                handlerForButton={handlerOpenModal}
                id={cats.id}
                image={cats.image}
                name={cats.name}
                price={cats.price}
                color={cats.color}
                updateStarCounter={props.updateStarCounter}
                starCounter={props.starCounter}
                favListHandler={props.favListHandler}
                favList={props.favList.includes(cats.id)}
                updateActiveCart={updateActiveCart}
              />
            </SwiperSlide>
          ))}
        </ul>
      </Swiper>
      {modal && (
        <Modal
          cartListHandler={props.cartListHandler}
          modal__styles="modal"
          btn__styles=" "
          closeModal={handlerCloseModals}
          closeModalBtn={true}
          title="Do you want to add this kitten to cart?"
          text="Your friend is waiting for you!"
          activeCart={activeCart}
        />
      )}
    </>
  );
}

CatList.defaultProps = {
  cats: PropTypes.array,
  isLoaded: PropTypes.bool,
  modal: PropTypes.bool,
  activeCart: PropTypes.string,
};

CatList.propTypes = {
  handlerOpenModal: PropTypes.func,
  handlerCloseModals: PropTypes.func,
  updateStarCounter: PropTypes.func,
  favListHandler: PropTypes.func,
  cartListHandler: PropTypes.func,
};

export default CatList;
