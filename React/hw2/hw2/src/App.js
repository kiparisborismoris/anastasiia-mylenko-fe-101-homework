import { Component } from "react";
import Header from "./components/Header/Header";
import CatList from "./components/CatList/CatList";
import DoggoPage from "./components/DoggoPage/DoggoPage";
import Footer from "./components/Footer/Footer";

class App extends Component {
  state = {
    favList: [],
    cartList: [],
  };

  // Star localStorage
  favListHandler = (e) => {
    const id = e.target.closest(".cat__card").id;
    this.setState(
      (prevState) => {
        if (!prevState.favList.includes(id)) {
          let newList = prevState.favList;
          newList.push(id);
          return {
            ...prevState,
            favList: newList,
          };
        } else {
          return {
            ...prevState,
            favList: prevState.favList.filter((el) => {
              if (el !== id) {
                return el;
              }
            }),
          };
        }
      },
      () => {
        localStorage.setItem("favList", JSON.stringify(this.state.favList));
      }
    );
  };

  componentDidMount() {
    let getfavList = localStorage.getItem("favList");
    if (getfavList !== null) {
      getfavList = JSON.parse(getfavList);

      this.setState((prevState) => {
        return { ...prevState, favList: getfavList };
      });
    }

    // same for Add to Cart
    let getCartList = localStorage.getItem("cartList");
    if (getCartList !== null) {
      getCartList = JSON.parse(getCartList);
      this.setState((prevState) => {
        return { ...prevState, cartList: getCartList };
      });
    }
  }

  // Cart localStorage
  cartListHandler = (id) => {
    this.setState(
      (prevState) => {
        if (!prevState.cartList.includes(id)) {
          let newCartList = prevState.cartList;
          newCartList.push(id);
          return {
            ...prevState,
            cartList: newCartList,
          };
        }
      },
      () => {
        localStorage.setItem("cartList", JSON.stringify(this.state.cartList));
      }
    );
  };

  // Delete item from cart ?
  // deleteItem = (e) => {
  //   const id = e.target.closest(".cat__card").id;
  //   this.setState(
  //     (prevState) => {
  //       let newCartList = prevState.cartList;
  //       newCartList = newCartList.filter((el) => {
  //         if (el !== id) {
  //           return el;
  //         }
  //       });
  //       return {
  //         ...prevState,
  //         cartList: newCartList,
  //       };
  //     },
  //     () => {
  //       localStorage.setItem("cartList", JSON.stringify(this.state.cartList));
  //     }
  //   );
  // };

  //increase counters

  render() {
    return (
      <>
        <Header
          counter={this.state.cartList.length}
          starCounter={this.state.favList.length}
        />
        <CatList
          className="main__page"
          favListHandler={this.favListHandler}
          favList={this.state.favList}
          cartListHandler={this.cartListHandler}
          cartList={this.state.cartList}
        />
        <DoggoPage />
        <Footer />
      </>
    );
  }
}

export default App;
