import { Component } from "react";
import "./Button.scss";
import PropTypes from "prop-types";

class Button extends Component {
  render() {
    return (
      <>
        <button
          className={this.props.btnStyles}
          type="button"
          onClick={this.props.btnAction}
        >
          {this.props.text}
        </button>
      </>
    );
  }
}

Button.defaultProps = {
  type: "button",
  btnAction: () => {},
};

Button.propTypes = {
  btnStyles: PropTypes.string,
  type: PropTypes.string,
  btnAction: PropTypes.func,
  text: PropTypes.string,
};

export default Button;
