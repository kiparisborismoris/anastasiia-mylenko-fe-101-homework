import { Component } from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import "./Modal.scss";

class Modal extends Component {
  render() {
    return (
      <div
        className="overlay"
        onClick={(event) => {
          if (!event.target.closest(".modal")) {
            this.props.closeModal();
          }
        }}
      >
        <div className={this.props.modal__styles}>
          <div className="modal__content">
            <h2 className="modal__title">{this.props.title}</h2>
            <p className="modal__text">{this.props.text}</p>
            <div className="btn__container">
              <Button
                btnStyles={this.props.btn__styles + " btn__good"}
                text="Add to cart"
                btnAction={() => {
                  this.props.closeModal();
                  this.props.cartListHandler(this.props.activeCart);
                }}
              />
              <Button
                btnStyles={this.props.btn__styles + " btn__bad"}
                text="Cancel"
                btnAction={this.props.closeModal}
              />
            </div>

            {this.props.closeModalBtn && (
              <button onClick={this.props.closeModal} className="modal__close">
                X
              </button>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;

Modal.defaultProps = {
  btn__styles: "btn__styles",
  modal__styles: "modal__styles",
  closeModalBtn: true,
};

Modal.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  closeModal: PropTypes.func,
  handlerModal: PropTypes.func,
  cartListHandler: PropTypes.func,
  activeCart: PropTypes.string,
};
