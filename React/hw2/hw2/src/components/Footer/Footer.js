import { Component } from "react";
import "./Footer.scss";

class Footer extends Component {
  render() {
    return (
      <div className="footer__container">
        <div className="footer__container--content">
          <p className="footer__container--content__text footer__text">All rights reserved ©</p>
        </div>
      </div>
    );
  }
}

export default Footer;
