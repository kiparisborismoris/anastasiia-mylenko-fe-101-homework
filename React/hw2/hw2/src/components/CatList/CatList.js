import { Component } from "react";
import CatCard from "../CatCard/CatCard";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";

import "swiper/css";
import "swiper/css/navigation";
import "./CatList.scss";

class CatList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cats: [],
      isLoaded: false,
      modal: false,
      activeCart: "",
    };
  }

  updateActiveCart = (value) => {
    this.setState({ activeCart: value });
  };

  componentDidMount() {
    fetch("./cats/cats.json")
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          cats: json,
          isLoaded: true,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  handlerOpenModal = (target) => {
    this.setState((prev) => (prev[target] = true));
  };

  handlerCloseModals = () => {
    this.setState(() => ({
      modal: false,
    }));
  };

  render() {
    const { isLoaded, cats } = this.state;
    if (!isLoaded) return <div className="error__load--state">Loading...</div>;

    return (
      <>
        <h1 className="catList__title">Kittens</h1>
        <Swiper
          navigation={true}
          className="catList__container"
          spaceBetween={50}
          slidesPerView={4}
          modules={[Navigation]}
        >
          <ul className="cats__container">
            {cats.map((cats) => (
              <SwiperSlide>
                <CatCard
                  handlerForButton={this.handlerOpenModal}
                  id={cats.id}
                  key={cats.id}
                  image={cats.image}
                  name={cats.name}
                  price={cats.price}
                  color={cats.color}
                  updateStarCounter={this.props.updateStarCounter}
                  starCounter={this.props.starCounter}
                  favListHandler={this.props.favListHandler}
                  favorite={this.props.favList.includes(cats.id)}
                  updateActiveCart={this.updateActiveCart}
                />
              </SwiperSlide>
            ))}
          </ul>
        </Swiper>
        {this.state.modal && (
          <Modal
            cartListHandler={this.props.cartListHandler}
            modal__styles="modal"
            btn__styles=" "
            closeModal={this.handlerCloseModals}
            closeModalBtn={true}
            title="Do you want to add this kitten to cart?"
            text="Your friend is waiting for you!"
            activeCart={this.state.activeCart}
          />
        )}
      </>
    );
  }
}

CatList.defaultProps = {
  cats: PropTypes.array,
  isLoaded: PropTypes.bool,
  modal: PropTypes.bool,
  activeCart: PropTypes.string,
};

CatList.propTypes = {
  handlerOpenModal: PropTypes.func,
  handlerCloseModals: PropTypes.func,
  updateStarCounter: PropTypes.func,
  favListHandler: PropTypes.func,
  cartListHandler: PropTypes.func,
};

export default CatList;
