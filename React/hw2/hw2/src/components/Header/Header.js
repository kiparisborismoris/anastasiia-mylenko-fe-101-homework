import { Component } from "react";
import PropTypes from "prop-types";
import "./Header.scss";
import { ReactComponent as Star } from "./Star.svg";

class Header extends Component {
  render() {
    return (
      <>
        <div className="header__container">
          <div className="header__container--content">
            <h2 className="top__header--logo logo">CATS</h2>
            <Star className="header__star" />
            <span className="star__counter">{this.props.starCounter}</span>
            <img
              className="header__shop--cart"
              src="./icons/shopping-cart.svg"
              alt="shopping-cart"
            />
            <span className="counter__output">{this.props.counter}</span>
          </div>
        </div>
      </>
    );
  }
}

Header.propTypes = {
  starCounter: PropTypes.number,
  counter: PropTypes.number,
};

export default Header;
