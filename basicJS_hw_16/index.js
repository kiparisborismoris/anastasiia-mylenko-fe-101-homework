function fib(F0, F1, n) {
  if (n === 0) {
    return F0;
  }

  if (n === 1) {
    return F1;
  }

  let current = 0;
  let prev1 = F1;
  let prev2 = F0;

  for (let i = 2; i <= Math.abs(n); i++) {
    current = prev1 + prev2;
    prev2 = prev1;
    prev1 = current;
  }

  if (n < 0 && n % 2 === 0) {
    current = -current;
  }

  return current;
}

const F0 = parseInt(prompt("Please input your number:"));
const F1 = parseInt(prompt("Please input your 2nd number:"));
const n = parseInt(prompt("Please input the serial Fibonacci number:"));

const result = alert(fib(F0, F1, n));
